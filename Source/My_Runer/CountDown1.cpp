// Fill out your copyright notice in the Description page of Project Settings.

#include "My_Runer.h"
#include "CountDown1.h"

// Sets default values
ACountDown1::ACountDown1()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACountDown1::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACountDown1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

